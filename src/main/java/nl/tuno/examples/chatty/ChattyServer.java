package nl.tuno.examples.chatty;

import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.ServerWebSocket;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.platform.Verticle;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple chat server with Vert.x using WebSockets.
 *
 * @author Denis Tunović
 */
public class ChattyServer extends Verticle {
    public void start() {
        final Logger logger = container.logger();
        final Map<String, ServerWebSocket> connectedClients = new HashMap<>();

        HttpServer server = vertx.createHttpServer();

        server.websocketHandler(new Handler<ServerWebSocket>() {
            @Override
            public void handle(final ServerWebSocket webSocket) {
                logger.info("Session established");
                final String clientId = webSocket.textHandlerID();
                connectedClients.put(clientId, webSocket);

                webSocket.dataHandler(new Handler<Buffer>() {
                    @Override
                    public void handle(Buffer data) {
                        String message = data.toString();
                        logger.info(message);

                        for (Map.Entry<String, ServerWebSocket> client : connectedClients.entrySet()) {
                            client.getValue().writeTextFrame(message);
                        }
                    }
                });

                webSocket.closeHandler(new Handler<Void>() {
                    @Override
                    public void handle(Void aVoid) {
                        connectedClients.remove(clientId);
                        logger.info("Session closed");
                    }
                });
            }
        }).requestHandler(new Handler<HttpServerRequest>() {
            @Override
            public void handle(HttpServerRequest request) {
                request.response().sendFile("web/" + request.path().substring(1));
            }
        }).listen(8080);
    }
}
